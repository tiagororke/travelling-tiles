import aws from 'aws-sdk';
import fs from 'fs';
import dotenv from 'dotenv';

// get enviroment variables
dotenv.config();
let sync_database = process.env.SYNC_DATABASE == 'false' ? false : true;

const s3 = new aws.S3({
   accessKeyId: process.env.AWS_ACCESS_KEY_ID,
   secretAccessKey: process.env.AWS_SECRET_KEY,
   Bucket: process.env.S3_BUCKET_NAME
});

// not sure why we need to pass the bucket name again, but hey
const aws_bucket = process.env.S3_BUCKET_NAME;

async function s3Download(file) {
   if(sync_database) {
      return new Promise((resolve, reject) => {
         s3.createBucket({Bucket: aws_bucket}, () => {
            s3.getObject({Bucket: aws_bucket, Key: file}, (err, data) => {
               if (err) {
                  reject(err);
               } else {
                  resolve(data);
               }
            });
         });
      });
   } else {
      return true;
   }
}

async function s3Upload(file, contents) {
   if(sync_database) {
      return new Promise((resolve, reject) => {
         s3.createBucket({Bucket: aws_bucket}, () => {
         s3.putObject({Bucket: aws_bucket, Key: file, Body: contents}, (err, data) => {
               if (err) {
                  reject(err);
               } else {
                  resolve(data);
               }
            });
         });
      });
   } else {
      return true;
   }
}

async function s3SyncFile(file, overwrite_local = false) {
   if(sync_database) {
      return new Promise((resolve, reject) => {
         fs.readFile(file, 'utf8', async (err, data) => {

            if (err) {
               if (err.code === 'ENOENT') {
                  // if file doesn't exist, get it from the bucket
                  console.log(file, 'not found, getting from S3 bucket...');
               } else {
                  // there some other problem reading the file
                  console.error(err);
                  return reject(err);
               }
            } else {
               // otherwise, upload it to the bucket unless overwrite_local
               if(overwrite_local) {
                  console.log(file, 'overwriting any local changes, getting from S3 bucket...');
               } else {
                  console.log('uploading ' + file + ' to S3 bucket...');
                  await s3Upload(file, data);
                  console.log(file + ' uploaded.');
                  // return false if local file already exists and we won't overwrite it
                  return resolve(false);
               }
            }

            let new_file = false;
            try {
               let s3_data = await s3Download(file);
               data = s3_data.Body;
            } catch (err) {
               // if file doesn't exist in the bucket, make a new empty file
               data = '';
               new_file = true;
               console.log(file, 'not found, creating a new empty file...');
            }
            await fs.writeFile(file, data, (err) => {
               if (err) {
                  // there some problem writing the file
                  console.error(err);
                  return reject(err);
               } else {
                  // return true if file is new or false if downloaded from s3
                  if(new_file) {
                     console.log(file + ' created.');
                     return resolve(true);
                  } else {
                     console.log(file + ' downloaded.');
                     return resolve(false);
                  }
               }
            });
         });
      });
   } else {
      return true;
   }
}

async function s3test() {
   console.log('s3 upload test...');
   let test = {'msg': 'hello s3'}
   let updata = await s3Upload('test.txt', JSON.stringify(test, null, 1));
   console.log(updata);
   console.log('s3 download test...')
   let data = await s3Download('test.txt');
   let data_p = JSON.parse(data.Body);
   console.log(data_p);
}

export { s3Download, s3Upload, s3SyncFile, s3test };