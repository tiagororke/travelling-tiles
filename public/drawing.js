

// -------------------------- DRAWING CAPTURE ------------------------------- //

// based on https://codepen.io/honmanyau/pen/OoOMQR
// and https://dev.to/javascriptacademy/create-a-drawing-app-using-javascript-and-canvas-2an1

const canvas = document.getElementById('drawing');
canvas.width = 567;
canvas.height = 567;
const min_vertex_distance = 5;

const strokeStyle = '#154096';
const lineWidth = 7;

const canvas_context = canvas.getContext('2d');

let paths = [];
let vertices = [];
let vertex_count = 0;
let new_paths = false;

const state = {
  drawing: false
};

const history_slider = document.getElementById('history-slider');
// history_slider.oninput = () => {
//    renderDrawing(history_slider.value);
// }
history_slider.onmouseup = () => {
   renderDrawing(history_slider.value);
}
history_slider.ontouchend = () => {
   renderDrawing(history_slider.value);
}

canvas.addEventListener('mousedown', captureStart);
canvas.addEventListener('mousemove', capture);
canvas.addEventListener('mouseup', captureEnd);
canvas.addEventListener('mouseout', captureEnd);
canvas.addEventListener('touchstart', captureStart);
canvas.addEventListener('touchmove', capture);
canvas.addEventListener('touchend', captureEnd);

async function captureStart(evt) {
   evt.preventDefault();
   if(history_slider.value < 1.0) {
      await truncateDrawing(history_slider.value);
   }
   const mousePos = getMousePos(evt);
   drawStart(canvas_context, mousePos.x, mousePos.y);
   drawStart(svg_context, mousePos.x, mousePos.y);
   vertices = [];
   state.drawing = true;
   new_paths = true;
}

function captureEnd(event) {
   event.preventDefault();
   if(state.drawing) {
      paths.push(vertices);
   }
   state.drawing = false;
}

function capture(event) {
   event.preventDefault();
   if (state.drawing) {
      let pvertex;
      const mousePos = getMousePos(event);
      if(vertices.length > 0) {
            pvertex= {
            x : vertices[vertices.length-1].x,
            y : vertices[vertices.length-1].y
         }
         if(distance(pvertex.x, pvertex.y, mousePos.x, mousePos.y) > min_vertex_distance) {
            addVertex(mousePos.x, mousePos.y);
         }
      } else {
         addVertex(mousePos.x, mousePos.y);
      }
   }
}

function addVertex(new_x, new_y) {
   vertices.push({
      x: new_x,
      y: new_y
   });
   vertex_count++;
   draw(canvas_context, new_x, new_y);
   draw(svg_context, new_x, new_y);
}

function getMousePos(evt) {
   let rect = canvas.getBoundingClientRect();
   let x = evt.clientX || evt.touches[0].clientX;
   let y = evt.clientY || evt.touches[0].clientY;
   return {
      x: x - rect.left,
      y: y - rect.top
  };
}

async function updateVertexCount() {
   vertex_count = 0;
   for (let path of paths) {
      vertex_count += path.length;
   }
}

async function truncateDrawing(range) {
   let vertex_range = vertex_count * Number(range);
   let index = 0;
   let path_slice = 0;
   let vertex_slice;
   vertex_counting:
      for (let path of paths) {
         vertex_slice = 0;
         for (let vertex of path) {
            if(index > vertex_range) {
               break vertex_counting;
            }
            index++
            vertex_slice++;
         }
         path_slice++;
      }
   paths[path_slice] = paths[path_slice].slice(0,vertex_slice);
   paths = paths.slice(0,path_slice+1);
   await updateVertexCount();
   history_slider.value = 1;
   renderDrawing(1);
}


// ------------------------------- STORAGE ---------------------------------- //

const parser = new DOMParser();

// using https://github.com/gliffy/canvas2svg to convert canvas contents to an svg element
const svg_context = new C2S(canvas.width,canvas.height); // "mock" canvas

// from https://github.com/F1LT3R/svg-to-dataurl
function svgToDataURI(svg) {
   const encoded = encodeURIComponent(svg)
      .replace(/'/g, '%27')
      .replace(/"/g, '%22');
   const header = 'data:image/svg+xml,';
   const data_uri = header + encoded;
   return data_uri;
}

async function loadDrawing(svg_data_uri) {
   paths = [];
   let svg_raw = decodeURIComponent(svg_data_uri);
   let svg_parsed = parser.parseFromString(
      svg_raw.substring(19), // remove header "data:image/svg+xml,"
      "image/svg+xml");
   let paths_raw = svg_parsed.getElementsByTagName('path');

   for (let path of paths_raw) {

      // unfortunately this doesn't seem to work with canvas2svg
      // let path_data = new Path2D(path.getAttribute('d'));
      // drawPath(canvas_context, path_data);
      // drawPath(svg_context, path_data);

      // so redrawing the paths vertex by vertex
      let path_data = path.getAttribute('d')
         .split(/[MmLlHhVvCcSsQqTtAa ]/)
         .filter(Number);
      let vertices = [];
      for (let i=0; i<path_data.length; i+=2) {
         vertices.push({
            x: path_data[i],
            y: path_data[i+1]
         });
      }
      paths.push(vertices);
   }
   await updateVertexCount();
   //renderDrawing(1);
   await animateDrawing();
}


// ------------------------------ RENDERING --------------------------------- //


function drawStart(ctx, x, y) {
   ctx.beginPath();
   ctx.moveTo(x, y);
   ctx.lineWidth = lineWidth;
   ctx.strokeStyle = strokeStyle;
   ctx.lineCap = 'round';
   ctx.stroke();
}

function draw(ctx, x, y) {
   ctx.lineTo(x, y);
   ctx.stroke();
}

// unused...
function drawPath(ctx, path) {
   ctx.lineWidth = lineWidth;
   ctx.strokeStyle = strokeStyle;
   ctx.lineCap = 'round';
   ctx.beginPath();
   ctx.stroke(path);
}

function renderDrawing(range) {
   canvas_context.clearRect(0, 0, canvas.width, canvas.height);
   svg_context.clearRect(0, 0, canvas.width, canvas.height);
   let vertex_range = vertex_count * Number(range);
   let index = 0;
   for (let path of paths) {
      drawStart(canvas_context, path[0].x, path[0].y);
      drawStart(svg_context, path[0].x, path[0].y);
      for (let vertex of path) {
         index++;
         if(index > vertex_range) break;
         draw(canvas_context, vertex.x, vertex.y);
         draw(svg_context, vertex.x, vertex.y);
      }
   }
}

async function animateDrawing() {
   for (let i=0.0; i<=1.0; i+= 0.05) {
      history_slider.value = i;
      renderDrawing(i);
      await sleep(100);
   }
   history_slider.value = 1.0;
   renderDrawing(1.0);
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function distance(x1,y1,x2,y2) {
   let a = x1 - x2;
   let b = y1 - y2;
   return Math.sqrt( a*a + b*b );
}

// ----------------------------- DEBUGGING ---------------------------------- //


// const testbtn = document.getElementById('test');
// testbtn.addEventListener('click', test);

function test(evt) {
   evt.preventDefault();
   // let a = svgToDataURI(svg_context.getSerializedSvg());
   // let b = decodeURIComponent(a);
   // let p = new DOMParser();
   // let c = p.parseFromString(b.substring(19), "image/svg+xml");
   // //console.log(c.firstChild.childNodes[1].childNodes[0].getAttribute('d'));
   // console.log(c.getElementsByTagName('path').getAttribute('d'));
   //document.getElementById("svg-img").src = svgToDataURI(svg_context.getSerializedSvg())
}
