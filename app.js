import path from 'path';
import express from 'express';
import createError from 'http-errors';
import { fileURLToPath } from 'url';
import dotenv from 'dotenv';
// import fileUpload from 'express-fileupload';
// import bodyParser from 'body-parser';

// get environment variables
dotenv.config();

import router from './routes.js';

// get dirname for use in ES module
// https://bobbyhadz.com/blog/javascript-dirname-is-not-defined-in-es-module-scope
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();

// views setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// routes setup
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({extended: false, limit: '50mb'})) // ---------------- // use body-parser instead ?
// app.use(bodyParser.urlencoded())
// app.use(fileUpload()); ---------------------------------------- // no uploading required?
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', router);
app.use("/media/", express.static(path.join(__dirname, 'public/media')));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {

   // set locals, only providing error in development
   res.locals.message = err.message;
   res.locals.error = req.app.get('env') === 'development' ? err : {};

   // render the error page
   res.status(err.status || 500);
   res.render('error');
});

export default app;