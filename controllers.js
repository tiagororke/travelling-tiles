import fs from 'fs';
import path from 'path';
import asyncHandler from 'express-async-handler';
import { body, validationResult } from 'express-validator';
//import { Parser } from '@json2csv/plainjs';
//import filenamify from 'filenamify';
//import stream from 'stream';

import dotenv from 'dotenv';
// get enviroment variables
dotenv.config();

import { database, tiles, topics, databaseSave } from './database.js';
const lang = JSON.parse(fs.readFileSync('./lang.json'));


// -------------------------------------------------------------------------- //

export const home = asyncHandler(async (req, res, next) => {

   // create array of topics from the database
   let topic_gallery = [];
   const topic_array = await topics.find({ 'name' : { '$ne' : null }});

   for (const topic of topic_array) {

      // get most recent 8 drawings from topic for thumbnail
      let thumbnails = [];
      let tile_array = await tiles.find({ 'topic_id' : { $aeq: topic.$loki } });
      tile_array = tile_array.reverse();
      let n = tile_array.length < 8 ? tile_array.length : 8;
      for(let i=0; i<n; i++) {
         thumbnails.push({ 'thumbnail' : tile_array[i].drawing });
      }
      topic_gallery.push({
         'name' : topic.name,
         'tiles' : thumbnails,
         'id' : topic.$loki
      });
   }

   // get first 8 drawings which have no topic
   let thumbnails = [];
   let tile_array = await tiles.find({ 'topic_id' : '-1' });
   tile_array = tile_array.reverse();
   let n = tile_array.length < 8 ? tile_array.length : 8;
   for(let i=0; i<n; i++) {
      thumbnails.push({ 'thumbnail' : tile_array[i].drawing });
   }
   topic_gallery.push({
      'name' : lang.no_topic,
      'tiles' : thumbnails,
      'id' : -1
   });


   // get any message content
   let msg;
   switch (req.query.msg) {
      case 'delete':
         msg = lang.home.deleted_topic + req.query.topic_id;
         break;
   }
   res.render('index', {
      lang: lang,
      topics: topic_gallery,
      msg: msg });
});


// -------------------------------------------------------------------------- //

export const topic = asyncHandler(async (req, res, next) => {

   let topic;

   // create array of tiles for this topic
   let tile_gallery = [];

   if(req.query.id >= 0) {

      // if there is a topic, get it from the from database
      topic = await topics.get(req.query.id);

      // and get the tiles for the corresponding topic
      const tile_array = await tiles.find({ 'topic_id' : req.query.id });
      tile_array.forEach((tile) => {
         tile_gallery.push({
            'thumbnail' : tile.drawing,
            'id' : tile.$loki
         });
      });

   } else {

      // if there is no topic, get the no_topic string as a label
      topic = {
         'name' : lang.no_topic,
         '$loki' : -1
      };

      // and get all tiles which do not have a topic
      const tile_array = await tiles.find({ 'topic_id' : '-1' });
      tile_array.forEach((tile) => {
         tile_gallery.push({
            'thumbnail' : tile.drawing,
            'id' : tile.$loki
         });
      });
   }

   // get any message content
   let msg;
   switch (req.query.msg) {
      case 'delete':
         msg = lang.home.deleted_tile + req.query.tile_id;
         break;
   }
   res.render('topic', {
      lang: lang,
      topic: topic,
      tiles: tile_gallery.reverse(),
      msg: msg });
});


// -------------------------------------------------------------------------- //

export const view = asyncHandler(async (req, res, next) => {

   let new_tile = 'true';
   let tile = null;

   // Get tile from database
   if(req.query.id >= 0) {
      tile = await tiles.get(req.query.id);
      new_tile = 0;
   } else {
      new_tile = 1;
   }

   // Get topic id
   let topic_id = new_tile > 0 ? req.query.topic_id : tile.topic_id;

   let topic;

   // Get topic
   if(topic_id >= 0) {
      topic = await topics.get(topic_id);
   } else {
      topic = {
         'name' : lang.no_topic,
         '$loki' : -1
      };
   }

   res.render('view', {
      lang: lang,
      topic: topic,
      new_tile: new_tile,
      tile: tile
   });
});


// -------------------------------------------------------------------------- //

export const publish = [

   // Validate and sanitize the author fields.
   body('author_name', lang.errors.name_error ).trim().notEmpty().escape(),
   body('author_age', lang.errors.age_error ).trim().notEmpty().escape().isNumeric(),

   // Process request after validation and sanitization.
   asyncHandler(async (req, res, next) => {

      // Extract the validation errors from a request.
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
         // There are errors. Render the form again with sanitized values/error messages.
         res.render('create', {
            lang: lang,
            tile: req.body.tile,
            errors: errors.array(),
         });
         return;

      } else {

         // If the data from form is valid, add new tile to the database

         // make a timestamp
         let timestamp = getTimestamp()

         // update the authors list
         let authors = [];
         if(req.body.new_tile == 0) { // if a forked tile
            // if author list has only one entry, convert it into an array
            let upstream_authors = Array.isArray(req.body.upstream_authors) ?
               req.body.upstream_authors :
               [req.body.upstream_authors];
            upstream_authors.forEach((author) => {
               authors.push(JSON.parse(author));
            });
         }
         authors.push({
               'name' : req.body.author_name,
               'age' : req.body.author_age,
               'timestamp' : timestamp,
            });

         // add to database
         let tile = tiles.insert({
               'drawing' : req.body.drawing_data,
               'authors' : authors,
               'upstream_id' : req.body.upstream_id,
               'topic_id' : req.body.topic_id
            });

         // Tile published. Redirect to topic.
         res.redirect('/topic?id=' + req.body.topic_id);
         databaseSave();

         console.log("----------------------------");
         console.log(
            req.body.new_tile == 1 ?
            'added new tile: ' + tile.$loki :
            'forked tile: ' + tile.upstream_id + ' into tile ' + tile.$loki
         );
         console.log("authors:", tile.authors);
         console.log("num tiles:", tiles.count());
      }
   }),
];


// -------------------------------------------------------------------------- //

export const create_topic = [

   // Validate and topic name field
   body('topic_name', lang.errors.name_error ).trim().notEmpty().escape(),

   // Process request after validation and sanitization.
   asyncHandler(async (req, res, next) => {

      // Extract the validation errors from a request.
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
         // There are errors. Render the form again with sanitized values/error messages.
         res.render('index', {
            lang: lang,
            topics: req.body.topics,
            errors: errors.array(),
         });
         return;

      } else {

         // If the data from form is valid, add new topic to the database

         // make a timestamp
         let timestamp = getTimestamp()

         // add to database
         let topic = topics.insert({
            'name' : req.body.topic_name,
            'timestamp' : timestamp
         });

         // Topic created. Redirect to homepage.
         res.redirect('/');
         databaseSave();

         console.log("----------------------------");
         console.log('added new topic:', topic.$loki, ':', topic.name);
      }
   }),
];


// -------------------------------------------------------------------------- //


function getTimestamp() {
   let time = new Date(Date.now());
   let timestamp =
      time.getFullYear() + '-' +
      String(time.getMonth()).padStart(2,'0') + '-' +
      String(time.getDate()).padStart(2,'0') + ' ' +
      String(time.getHours()).padStart(2,'0') + ':' +
      String(time.getMinutes()).padStart(2,'0');
   return timestamp;
}