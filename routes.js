import express from 'express';
import * as controller from './controllers.js';

const router = express.Router();

/* redirect trailing slashes */
// https://stackoverflow.com/questions/13442377/redirect-all-trailing-slashes-globally-in-express
router.get('\\S+\/$', function (req, res) {
   return res.redirect(301, req.path.slice(0, -1) + req.url.slice(req.path.length));
});

router.get('/', controller.home);
router.get('/topic', controller.topic);
router.get('/view', controller.view);
router.post('/publish', controller.publish);
router.post('/create_topic', controller.create_topic);

export default router;