import loki from 'lokijs';
import { s3SyncFile } from './aws-s3.js';

let tiles, topics;

const db = new loki('database.json', {
   autoload: true,
   throttledSaves: true,
   autoloadCallback : databaseInit
});

async function databaseInit() {

   console.log("sync database: ", process.env.SYNC_DATABASE);

   // first, sync database with s3, overwriting any local database
   await s3SyncFile('database.json', true).then((new_db) => {

      db.loadDatabase({}, (err) => {
         if (err) {
            console.error("error : " + err);
         } else {
            tiles = db.getCollection("tiles");
            if (!tiles) {
               tiles = db.addCollection("tiles");
            }
            topics = db.getCollection("topics");
            if (!topics) {
               topics = db.addCollection("topics");
            }

            // save database, and sync with s3 if new
            databaseSave(new_db);

            console.log('database initialised with', topics.count(), 'topics and', tiles.count(), 'tiles');
         }
      });
   });
}

function databaseSave(sync_s3 = true) {
   db.saveDatabase(() => {
      if(sync_s3) {
         s3SyncFile('database.json')
      }
   });
}

// flush database on program exit
process.on('SIGINT', function() {
   db.close();
});

export { db as database, tiles, topics, databaseSave };